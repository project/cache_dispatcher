<?php
// $Id$
/**
 * Thomas Gielfeldt <thomas@gielfeldt.com>
 * Copyright 2011
 * 
 * @file
 * 
 * Provides multiple caching backends framework. Use any drupal 6 cache.inc file for a given cache table.
 * This module works by generating php source-code in order to separate cache_get(), and so on, into different
 * "namespaces" by prefixing the function names.
 * 
 * As this module auto-generates code, it might not be considered production-grade. Use with caution.
 *
 * The following example uses the memcache backend for all cache tables except cache_form:
 *
 * @code
 * $conf['cache_inc'] = './includes/cache_dispatcher.inc'
 * $conf['cache_incs'] = array(
 *   'default'    => './sites/all/modules/contrib/memcache/memcache.inc', // 'default' is used for tables not defined in this array
 *   'cache_form' => './includes/cache.inc',
 * );
 * @endcode
 *
 * @todo Add locking and atomicity to cache file generation (is this necessary?)
 */

/**
 * Get entry from cache
 *
 * @param string $cid
 * @param string $table
 * @return mixed
 */
function cache_get($cid, $table = 'cache') {
  $prefix = cache_load_backend($table);
  $function = "{$prefix}_cache_get";
  return $function($cid, $table);
}

/**
 * Get multiple entries from cache
 *
 * @param string $cids
 * @param string $table
 * @return array
 */
function cache_get_multiple($cids, $table = 'cache') {
  $prefix = cache_load_backend($table);
  $function = "{$prefix}_cache_get_multiple";
  if (function_exists($function)) {
    return $function($cids, $table);
  }
  else {
    $result = array();
    foreach ($cids as $cid) {
      $result[$cid] = cache_get($cid, $table);
    }
    return $result;
  }
}

/**
 * Set a cache entry
 *
 * @param string $cid
 * @param mixed $data
 * @param string $table
 * @param integer $expire
 * @param array $headers
 * @return boolean
 */
function cache_set($cid, $data, $table = 'cache', $expire = CACHE_PERMANENT, $headers = NULL) {
  $prefix = cache_load_backend($table);
  $function = "{$prefix}_cache_set";
  return $function($cid, $data, $table, $expire, $headers);
}

/**
 * Clear one or more cache entries
 *
 * @param string $cid
 * @param string $table
 * @param boolean $wildcard
 * @return boolean
 */
function cache_clear_all($cid = NULL, $table = NULL, $wildcard = FALSE) {
  $prefix = cache_load_backend($table);
  $function = "{$prefix}_cache_clear_all";
  return $function($cid, $table, $wildcard);
}

/**
 * Load cache extension
 *
 * @staticvar array $extensions
 * @param string $table
 *   table to acquire namespace for
 * @return string
 *   namespace for table
 */
function cache_load_backend($table) {
  static $prefixes = array();
  static $included = array();

  $backends = variable_get('cache_incs', array());

  // Fallback to default if no routing is specified for the table
  if (!isset($backends[$table])) {
    $table = 'default';
  }

  // Fallback to default cache backend if default is not specified
  if (!isset($backends[$table])) {
    $backends[$table] = './includes/cache.inc';
  }

  // CWD changes when inside a shutdown function. Revert to DOCUMENT_ROOT in order for realpath() to work.
  $cwd = getcwd();
  chdir($_SERVER['DOCUMENT_ROOT']);
  
  // Generate filename
  $file = realpath($backends[$table]);
  $prefix = md5($file);
  
  // Where to put it. "cache_dispatcher_cache_directory" if defined, otherwise system temp dir.
  // Drupal temp not possible, as cache is called before it's declared (if declared in DB).
  // NOTE: "cache_dispatcher_cache_directory" should only be declared in settings.php NOT in DB.
  if (!$cache_directory = variable_get('cache_dispatcher_cache_directory', NULL)) {
    $cache_directory = sys_get_temp_dir();
  }
  $cache_file = $cache_directory . '/cache_dispatcher_' . $prefix . '.inc';
  // Change back to where we came from
  chdir($cwd);

  // Have we already loaded this extension?
  if (isset($prefixes[$file])) {
    return $prefixes[$file];
  }

  // Setup prefix
  $prefix = '__cache_dispatcher__' . $prefix;
  $prefixes[$file] = $prefix;

  // Load the cache file
  $include_path = get_include_path();
  set_include_path($include_path . PATH_SEPARATOR . dirname($file));

  // Do we already have it?
  if (file_exists($cache_file)) {
    require $cache_file;
    set_include_path($include_path);
    return $prefix;
  }
  
  // Cache file doesn't exist, generate it
  $code = file_get_contents($file);
  $code = preg_replace('/(function)\s+(cache_get|cache_set|cache_get_multiple|cache_clear_all)\s*\(/', '$1 ' . $prefix . '_$2(', $code);
  $temp_file = tempnam($cache_directory, 'cache_dispatcher');
  file_put_contents($temp_file, $code);
  require $temp_file;
  set_include_path($include_path);

  // Move the temp file into place
  @unlink($cache_file);
  @rename($temp_file, $cache_file);
  
  return $prefix;
}
